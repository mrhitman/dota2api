import "antd/dist/antd.css";
import React from "react";
import "./App.css";
import HeroesList from "./Components/HeroesList";
import Layout from "./Components/Layout";
import HeroesItem from "./Components/HeroesItem";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

function App() {
  return (
    <Layout>
      <Router>
        <Switch>
          <Route path="/hero/:id">
            <HeroesItem
              name="Anti Mage"
              id={1}
              img="/apps/dota2/images/heroes/antimage_full.png?"
            />
          </Route>
          <Route path="/">
            <HeroesList />
          </Route>
        </Switch>
      </Router>
    </Layout>
  );
}

export default App;
