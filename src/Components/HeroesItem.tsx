import React, {Component} from "react";

interface HeroesItemProps {
  id: number;
  name: string;
  img: string;
}

interface HeroesItemState {
  gamesPlayed: number;
  wins: number;
}

type MatchupsResponse = Array<{
  hero_id: number;
  games_played: number;
  wins: number;
}>;

export class HeroesItem extends Component<HeroesItemProps, HeroesItemState> {
  public componentDidMount() {
    return this.fetchData();
  }

  public state: HeroesItemState = {
    gamesPlayed: 0,
    wins: 0,
  };

  public async fetchData() {
    const url = `https://api.opendota.com/api/heroes/${this.props.id}/matchups`;
    const data: MatchupsResponse = await fetch(url, {
      method: "GET",
    }).then((data) => data.json());
    const hero = data.find((item) => this.props.id === item.hero_id);

    if (hero) {
      this.setState({
        gamesPlayed: hero.games_played,
        wins: hero.wins,
      });
    }
  }

  public render() {
    return (
      <>
        <img
          style={{width: 178, display: "block"}}
          src={"https://api.opendota.com" + this.props.img}
          alt={this.props.name}
        />
        {JSON.stringify(this.state)}
        {this.props.name}
      </>
    );
  }
}

export default HeroesItem;
