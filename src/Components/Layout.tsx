import React from 'react';
import { Layout as L } from 'antd';

export class Layout extends React.Component {
  public render() {
    return (
      <L>
        <L.Header>Heroes</L.Header>
        <L.Content>{this.props.children}</L.Content>
        <L.Footer>Copyright 2020</L.Footer>
      </L>
    );
  }
}

export default Layout;
