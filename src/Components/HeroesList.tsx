import React, {Component} from "react";
import {Row, Col} from "antd";
import {Link} from "react-router-dom";

interface HeroesListState {
  heroes: Array<{
    id: number;
    name: string;
    localized_name: string;
    img: string;
    icon: string;
  }>;
}

export class HeroesList extends Component<{}, HeroesListState> {
  public componentDidMount() {
    return this.fetchData();
  }

  public state: HeroesListState = {
    heroes: [],
  };

  public async fetchData() {
    const url = "https://api.opendota.com/api/heroStats";
    const data = await fetch(url, {method: "GET"}).then((data) => data.json());
    this.setState({heroes: data});
  }

  public render() {
    return (
      <Row gutter={[8, 8]}>
        {this.state.heroes.map((item) => (
          <Col span={3} xl={3} md={4} sm={6} key={item.id}>
            <img
              style={{width: 178, display: "block"}}
              src={"https://api.opendota.com" + item.img}
              alt={item.name}
            />
            <Link to={"/hero/" + item.id}>{item.localized_name}</Link>
          </Col>
        ))}
      </Row>
    );
  }
}

export default HeroesList;
